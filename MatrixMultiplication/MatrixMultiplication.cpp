// MatrixMultiplication.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include "MatrixMultiplication.h"
#include <limits>

/**the printMatrix function takes a matrix of any type and size and prints it to the console*/
template <typename T, size_t matrixRows, size_t matrixColumns>
void printMatrix(T(&matrix)[matrixRows][matrixColumns]) {

	for (int row = 0; row < matrixRows; ++row) {
		for (int column = 0; column < matrixColumns; ++column) {
			std::cout << matrix[row][column] << " ";

			if (column == matrixColumns - 1) {
				std::cout << "\n" << std::endl;
			}
		}
	}
}

/**The checkforOverflowOrUnderflow function takes two values of any type and checks if they will overflow or underflow when mutliplied to one another. 
 * IF they will overflow/underflow, checkforOverflowOrUnderflow prints a suitable error message and then returns true
 * ELSE checkforOverflowOrUnderflow returns false.*/
template <typename T>
bool checkforOverflowOrUnderflow(T value1, T value2) {

	bool willOverflowOrUnderflow = false;
	T maxValue = std::numeric_limits<int>::max();
	T minValue = std::numeric_limits<int>::min();

	//if statements to check the type of T and change the max and min values if T is anything other than int
	if (std::is_same_v<T, double>) {
		maxValue = std::numeric_limits<double>::max();
		minValue = std::numeric_limits<double>::min();
	} 
	else if (std::is_same_v<T, float>) {
		maxValue = std::numeric_limits<float>::max();
		minValue = std::numeric_limits<float>::min();
	}

	//if statements to check for overlow/underflow
	if (value1 > 0 && value2 > 0 && value1 > maxValue / value2) {
		std::cerr << "Error:" << std::endl;
		std::cerr << value1 << " times by " << value2 << " will overflow" << std::endl;
		willOverflowOrUnderflow = true;
	}
	else if (value1 < 0 && value2 > 0 && value1 < minValue / value2) {
		std::cerr << "Error:" << std::endl;
		std::cerr << value1 << " times by " << value2 << " will underflow" << std::endl;
		willOverflowOrUnderflow = true;
	}
	else if (value1 > 0 && value2 < 0 && value2 < minValue / value1) {
		std::cerr << "Error:" << std::endl;
		std::cerr << value1 << " times by " << value2 << " will underflow" << std::endl;
		willOverflowOrUnderflow = true;
	}
	else if (value1 < 0 && value2 < 0 && value1 < maxValue / value2) {
		std::cerr << "Error:" << std::endl;
		std::cerr << value1 << " times by " << value2 << " will overflow" << std::endl;
		willOverflowOrUnderflow = true;
	}

	return willOverflowOrUnderflow;
}

/**the multiplyTwoMatrices function takes 2 matrices of any type and size, checks that they can be multiplied and then either:
 * multiplies them and returns the product matrix (if they can be multiplied)
 * OR 
 * returns a suitable error message
 */
template <typename T, size_t matrix1Rows, size_t matrix1Columns, size_t matrix2Rows, size_t matrix2Columns>
void multiplyTwoMatrices(T(&matrix1)[matrix1Rows][matrix1Columns], T(&matrix2)[matrix2Rows][matrix2Columns]) {

	std::cout << "Matrix 1:" << std::endl;
	printMatrix(matrix1);
	std::cout << "Matrix 2:" << std::endl;
	printMatrix(matrix2);

	bool productMatrixWouldOverflowOrUnderflow = false;

	if (matrix1Columns != matrix2Rows) {
		std::cerr << "Error:" << std::endl;
		std::cerr << "Matrix 1 has " << matrix1Columns << " columns and matrix 2 has " << matrix2Rows << " rows" << std::endl;
		std::cerr << "Matrix 1 must have the same number of columns as matrix 2 has rows" << std::endl;
		std::cerr << "or else the two matrices cannot be multiplied!" << std::endl;
	}
	else if (std::is_same_v<T, char>) {
		std::cerr << "Error:" << std::endl;
		std::cerr << "char matrices can't be multiplied!" << std::endl;
	}
	//The code won't compile if you pass std::string matrices into this function because of the use of the "=" operator which is seen as "ambiguous" when dealing with std::string objects
	//The code inside the else if below this comment was one way that I tried to implement std::string error handling. It didn't work.
	else if (std::is_same_v<T, std::string>) {
		std::cerr << "Error:" << std::endl;
		std::cerr << "std::string matrices can't be multiplied!" << std::endl;
	}
	else {
		T productMatrix[matrix1Rows][matrix2Columns];

		for (int i = 0; i < matrix1Rows; ++i) {
			for (int j = 0; j < matrix2Columns; ++j) {
				productMatrix[i][j] = 0;
			}
		}

		for (int i = 0; i < matrix1Rows; ++i) {
			for (int j = 0; j < matrix2Columns; ++j) {
				for (int k = 0; k < matrix1Columns; ++k) {

					productMatrixWouldOverflowOrUnderflow = checkforOverflowOrUnderflow(matrix1[i][k], matrix2[k][j]);
					if (productMatrixWouldOverflowOrUnderflow) break;
					productMatrix[i][j] += matrix1[i][k] * matrix2[k][j];
				}
				if (productMatrixWouldOverflowOrUnderflow) break;
			}
			if (productMatrixWouldOverflowOrUnderflow) break;
		}

		if (!productMatrixWouldOverflowOrUnderflow) {
			std::cout << "Product Matrix:" << std::endl;
			printMatrix(productMatrix);
		}
	}
	std::cout << "------------------------------------------------------------------------------" << std::endl;
}

/**I attempted to overload multiplyTwoMatrices for the case whereby the user passes in std::string matrices. The version of multiplyTwoMatrices above
 * that uses a typename template will not compile if you try to call it by passing in two std::string matrices due to the use of the "=" operator which
 * is seen as "ambiguous" when dealing with the std::string type. Regardless, this overloaded function did not solve the problem. The code still won't 
 * compile. So this method is just here to show what my method of trying to get around that issue looked like.*/
template <size_t matrix1Rows, size_t matrix1Columns, size_t matrix2Rows, size_t matrix2Columns>
void multiplyTwoMatrices(std::string matrix1[matrix1Rows][matrix1Columns], std::string matrix2[matrix2Rows][matrix2Columns]) {

	std::cout << "Matrix 1:" << std::endl;
	printMatrix(matrix1);
	std::cout << "Matrix 2:" << std::endl;
	printMatrix(matrix2);

	std::cerr << "Error:" << std::endl;
	std::cerr << "std::string matrices can't be multiplied!" << std::endl;
}

/*Actual code ends here*/

/*I couldn't figure out how to use Google test so I wrote functions to act as faux unit tests from here onwards*/

//NEED A NULL MATRIX TEST HERE:

void positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive() {

	int m1[2][3] = {
		{1, 2, 3},
		{4, 5, 6}
	};
	int m2[3][2] = {
		{1, 2},
		{3, 4},
		{5, 6}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive() {
	int m1[3][2] = {
		{1, 2},
		{3, 4},
		{5, 6}
	};
	int m2[2][3] = {
		{-1, -2, -3},
		{-4, -5, -6}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative() {
	int m1[2][3] = {
		{-1, -2, -3},
		{-4, -5, -6}
	};
	int m2[3][2] = {
		{-1, -2},
		{-3, -4},
		{-5, -6}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_intMatrices_3x3times3x3_multiplyTwoMatrices() {
	int m1[3][3] = {
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9}
	};
	int m2[3][3] = {
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_intMatrices_2x2times3x3_multiplyTwoMatrices() {
	int m1[2][2] = {
		{1, 2},
		{3, 4}
	};
	int m2[3][3] = {
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_integerOverflow_multiplyTwoMatrices_bothPositive() {
	int m1[3][3] = {
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()}
	};
	int m2[3][3] = {
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_integerOverflow_multiplyTwoMatrices_bothNegative() {
	int m1[3][3] = {
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()}
	};
	int m2[3][3] = {
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_integerUnderflow_multiplyTwoMatrices_matrix1Positive() {
	int m1[3][3] = {
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()}
	};
	int m2[3][3] = {
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_integerUnderflow_multiplyTwoMatrices_matrix1Negative() {
	int m1[3][3] = {
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()},
		{std::numeric_limits<int>::min(), std::numeric_limits<int>::min(), std::numeric_limits<int>::min()}
	};
	int m2[3][3] = {
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()},
		{std::numeric_limits<int>::max(), std::numeric_limits<int>::max(), std::numeric_limits<int>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive() {
	double m1[2][3] = {
		{1.5, 2.5, 3.5},
		{4.5, 5.5, 6.5}
	};
	double m2[3][2] = {
		{1.5, 2.5},
		{3.5, 4.5},
		{5.5, 6.5}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive() {
	double m1[3][2] = {
		{1.5, 2.5},
		{3.5, 4.5},
		{5.5, 6.5}
	};
	double m2[2][3] = {
		{-1.5, -2.5, -3.5},
		{-4.5, -5.5, -6.5}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative() {
	double m1[2][3] = {
		{-1.5, -2.5, -3.5},
		{-4.5, -5.5, -6.5}
	};
	double m2[3][2] = {
		{-1.5, -2.5},
		{-3.5, -4.5},
		{-5.5, -6.5}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_doubleMatrices_3x3times3x3_multiplyTwoMatrices() {
	double m1[3][3] = {
		{1.5, 2.5, 3.5},
		{4.5, 5.5, 6.5},
		{7.5, 8.5, 9.5}
	};
	double m2[3][3] = {
		{1.5, 2.5, 3.5},
		{4.5, 5.5, 6.5},
		{7.5, 8.5, 9.5}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_doubleMatrices_2x2times3x3_multiplyTwoMatrices() {
	double m1[2][2] = {
		{1.5, 2.5},
		{3.5, 4.5}
	};
	double m2[3][3] = {
		{1.5, 2.5, 3.5},
		{4.5, 5.5, 6.5},
		{7.5, 8.5, 9.5}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_doubleOverflow_multiplyTwoMatrices_bothPositive() {
	double m1[3][3] = {
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()}
	};
	double m2[3][3] = {
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_doubleOverflow_multiplyTwoMatrices_bothNegative() {
	double m1[3][3] = {
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()}
	};
	double m2[3][3] = {
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_doubleUnderflow_multiplyTwoMatrices_matrix1Positive() {
	double m1[3][3] = {
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()}
	};
	double m2[3][3] = {
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_doubleUnderflow_multiplyTwoMatrices_matrix1Negative() {
	double m1[3][3] = {
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()},
		{std::numeric_limits<double>::min(), std::numeric_limits<double>::min(), std::numeric_limits<double>::min()}
	};
	double m2[3][3] = {
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()},
		{std::numeric_limits<double>::max(), std::numeric_limits<double>::max(), std::numeric_limits<double>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive() {
	float m1[2][3] = {
		{1.5f, 2.5f, 3.5f},
		{4.5f, 5.5f, 6.5f}
	};
	float m2[3][2] = {
		{1.5f, 2.5f},
		{3.5f, 4.5f},
		{5.5f, 6.5f}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive() {
	float m1[3][2] = {
		{1.5f, 2.5f},
		{3.5f, 4.5f},
		{5.5f, 6.5f}
	};
	float m2[2][3] = {
		{-1.5f, -2.5f, -3.5f},
		{-4.5f, -5.5f, -6.5f}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative() {
	float m1[2][3] = {
		{-1.5f, -2.5f, -3.5f},
		{-4.5f, -5.5f, -6.5f}
	};
	float m2[3][2] = {
		{-1.5f, -2.5f},
		{-3.5f, -4.5f},
		{-5.5f, -6.5f}
	};

	multiplyTwoMatrices(m1, m2);
}

void positiveTest_floatMatrices_3x3times3x3_multiplyTwoMatrices() {
	float m1[3][3] = {
		{1.5f, 2.5f, 3.5f},
		{4.5f, 5.5f, 6.5f},
		{7.5f, 8.5f, 9.5f}
	};
	float m2[3][3] = {
		{1.5f, 2.5f, 3.5f},
		{4.5f, 5.5f, 6.5f},
		{7.5f, 8.5f, 9.5f}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_floatMatrices_2x2times3x3_multiplyTwoMatrices() {
	float m1[2][2] = {
		{1.5f, 2.5f},
		{3.5f, 4.5f}
	};
	float m2[3][3] = {
		{1.5f, 2.5f, 3.5f},
		{4.5f, 5.5f, 6.5f},
		{7.5f, 8.5f, 9.5f}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_floatOverflow_multiplyTwoMatrices_bothPositive() {
	float m1[3][3] = {
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()}
	};
	float m2[3][3] = {
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_floatOverflow_multiplyTwoMatrices_bothNegative() {
	float m1[3][3] = {
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()}
	};
	float m2[3][3] = {
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_floatUnderflow_multiplyTwoMatrices_matrix1Positive() {
	float m1[3][3] = {
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()}
	};
	float m2[3][3] = {
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_floatUnderflow_multiplyTwoMatrices_matrix1Negative() {
	float m1[3][3] = {
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()},
		{std::numeric_limits<float>::min(), std::numeric_limits<float>::min(), std::numeric_limits<float>::min()}
	};
	float m2[3][3] = {
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()},
		{std::numeric_limits<float>::max(), std::numeric_limits<float>::max(), std::numeric_limits<float>::max()}
	};

	multiplyTwoMatrices(m1, m2);
}

void negativeTest_charMatrices_2x3times3x2_multiplyTwoMatrices() {
	char m1[2][3] = {
		{'a', 'b', 'c'},
		{'d', 'e', 'f'}
	};
	char m2[3][2] = {
		{'a', 'b'},
		{'c', 'd'},
		{'e', 'f'}
	};

	multiplyTwoMatrices(m1, m2);
}

/*Couldn't get this to work - as soon as I implemented the std::string error checking, my code wouldn't compile*/
//void negativeTest_stringMatrices_2x3times3x2_multiplyTwoMatrices() {
//	std::string m1[2][3] = {
//		{"Hello", "how", "are"},
//		{"you", "today", "?"}
//	};
//	std::string m2[3][2] = {
//		{"I'm", "good"},
//		{"thankyou", "how"},
//		{"about", "you?"}
//	};
//
//	multiplyTwoMatrices(m1, m2);
//}

int main()
{
	//I couldn't get Google test to work so I wrote functions to act as faux unit tests:
	//int matrices:
	positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive();
	positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive();
	positiveTest_intMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative();
	positiveTest_intMatrices_3x3times3x3_multiplyTwoMatrices();
	negativeTest_intMatrices_2x2times3x3_multiplyTwoMatrices();
	negativeTest_integerOverflow_multiplyTwoMatrices_bothPositive();
	negativeTest_integerOverflow_multiplyTwoMatrices_bothNegative();
	negativeTest_integerUnderflow_multiplyTwoMatrices_matrix1Positive();
	negativeTest_integerUnderflow_multiplyTwoMatrices_matrix1Negative();
	//double matrices:
	positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive();
	positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive(); //doesn't work - says that 1.5*-1.5 would underflow. It shouldn't
	positiveTest_doubleMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative();
	positiveTest_doubleMatrices_3x3times3x3_multiplyTwoMatrices();
	negativeTest_doubleMatrices_2x2times3x3_multiplyTwoMatrices();
	negativeTest_doubleOverflow_multiplyTwoMatrices_bothPositive();
	negativeTest_doubleOverflow_multiplyTwoMatrices_bothNegative(); //doesn't work - should display error message for overflow but it doesn't
	negativeTest_doubleUnderflow_multiplyTwoMatrices_matrix1Positive(); //doesn't work - should display error message for underflow but it doesn't
	negativeTest_doubleUnderflow_multiplyTwoMatrices_matrix1Negative(); //doesn't work - should display error message for underflow but it doesn't
	//float matrices:
	positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_bothPositive();
	positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_matrix1Positive(); //doesn't work - says that 1.5*-1.5 would underflow. It shouldn't
	positiveTest_floatMatrices_2x3times3x2_multiplyTwoMatrices_bothNegative();
	positiveTest_floatMatrices_3x3times3x3_multiplyTwoMatrices();
	negativeTest_floatMatrices_2x2times3x3_multiplyTwoMatrices();
	negativeTest_floatOverflow_multiplyTwoMatrices_bothPositive();
	negativeTest_floatOverflow_multiplyTwoMatrices_bothNegative(); //doesn't work - should display error message for overflow but it doesn't
	negativeTest_floatUnderflow_multiplyTwoMatrices_matrix1Positive(); //doesn't work - should display error message for underflow but it doesn't
	negativeTest_floatUnderflow_multiplyTwoMatrices_matrix1Negative(); //doesn't work - should display error message for underflow but it doesn't
	//non-numeric arrays (should error):
	negativeTest_charMatrices_2x3times3x2_multiplyTwoMatrices();
	//negativeTest_stringMatrices_2x3times3x2_multiplyTwoMatrices(); //couldn't get this to work - as soon as I implemented the std::string error checking, my code wouldn't compile
}